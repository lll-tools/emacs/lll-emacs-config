#!/bin/bash
#
# Copyright (C) 2021 Luis L. Lazaro <lll@luislain.com>

# When compiling Emacs-26 and below with brew in MacOSX
if [ -d /usr/local/opt/texinfo/bin ]; then
    export PATH=/usr/local/opt/texinfo/bin:$PATH
fi

SCRIPT_FILE="${0##*/}"
LOG_FILE="${SCRIPT_FILE%.*}.log"

## Colors
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NOCOLOR='\033[0m'

## Functions
log_exit () {
    if [ $1 -eq 0 ]; then
        echo -e "[${GREEN}OK${NOCOLOR}] $2"
    else
        if [ $1 -eq 2 ]; then
            echo -e "[${YELLOW}WARN${NOCOLOR}] $2"
        else
            echo -e "[${RED}ERR${NOCOLOR}] $2"
            exit 1
        fi
    fi
}

if ! case "$1" in emacs*) false;; esac; then
    EMACS_DEFAULT_VERSION="$1"
else
    EMACS_DEFAULT_VERSION="emacs-28"
fi

# Clone the master repository
if [ ! -d emacs ]; then
    git clone https://git.savannah.gnu.org/git/emacs.git
    #git clone https://savannah.gnu.org/projects/emacs/
    #git clone git://git.savannah.gnu.org/emacs.git
    #git clone git://git.sv.gnu.org/emacs.git
    # git config user.name 'Your Name'
    # git config user.email 'your.name@example.com'
    # git config transfer.fsckObjects true
    cd emacs
    git checkout $EMACS_DEFAULT_VERSION
    cd -
else
    echo "Emacs repository already cloned"
    cd emacs
    git checkout $EMACS_DEFAULT_VERSION
    cd -
fi

# Reset branch to compile
cd emacs
log_exit $?
#
git status
log_exit $?
git reset --hard
log_exit $?
git clean -xdf
log_exit $?
git pull
log_exit $?
git status
log_exit $?

#EMACS_BRANCH=$(git name-rev --name-only HEAD)
#EMACS_BRANCH=`git describe --all | cut -d '/' -f2`
EMACS_BRANCH_REV=$(git rev-parse --abbrev-ref HEAD)
EMACS_GIT_ROOT_DIR=$(git rev-parse --show-toplevel)
echo "Emacs Branch Rev: $EMACS_BRANCH_REV"
EMACS_BRANCH=`git branch | sed -n 's/*//1p'`
echo "Emacs Branch: $EMACS_BRANCH"
EMACS_BRANCH_DIR=${EMACS_BRANCH##* }
EMACS_BRANCH_DIRNAME=${EMACS_BRANCH_DIR%%)*}
echo "Emacs Branch Dirname: ${EMACS_BRANCH_DIRNAME}"
#RSYNC_DATE=$(date +%Y%m%d%H%M%S)
EMACS_COMPILE_DATE=$(date "+%Y-%m-%d")
EMACS_BRANCH_DIRNAME_NEW="${EMACS_BRANCH_DIRNAME}-${EMACS_COMPILE_DATE}"
EMACS_BRANCH_DIRNAME_FULL="mybuild-${EMACS_BRANCH_DIRNAME_NEW}"
echo "Emacs Branch Dirname Full: ${EMACS_BRANCH_DIRNAME_FULL}"
cd -

if [ ! -d emacs-${EMACS_BRANCH_DIRNAME_FULL} ]; then
    mkdir "emacs-${EMACS_BRANCH_DIRNAME_FULL}"
else
    # Build Dir
    echo "Directory emacs-${EMACS_BRANCH_DIRNAME_FULL} already exists."
    cd emacs-${EMACS_BRANCH_DIRNAME_FULL}
    make clean
    cd -
    # Build Tag
    cd emacs
    git tag -d ${EMACS_BRANCH_DIRNAME_FULL}
    cd -
fi

#
cd emacs
./autogen.sh | tee ../emacs-${EMACS_BRANCH_DIRNAME_FULL}/emacs-autogen.log
log_exit ${PIPESTATUS[0]}
cd -

XDG_CONFIG_DIR=${XDG_CONFIG_HOME:-$HOME/.config}
EMACS_RUN_DIR="${XDG_CONFIG_DIR}/emacs/bin/${EMACS_BRANCH_DIRNAME_FULL}"
mkdir -pv $EMACS_RUN_DIR
cd emacs-${EMACS_BRANCH_DIRNAME_FULL}
log_exit $?

# https://issuehint.com/issue/org-roam/org-roam/2210
# --without-imagemagick
# --with-no-titlebar
# --with-pdumper
# --with-xwidgets
# --with-pop
# --with-mailutils
# --without-dbus
# --with-ns
# --with-x-toolkit=athena
# CC=/usr/bin/clang ./configure
# --without-ns
# --without-x
# --with-gnutls=no

[ -f ../emacs/configure ] \
    && ../emacs/configure --prefix=${EMACS_RUN_DIR} \
			  --with-xft \
			  --with-x-toolkit=athena \
	| tee emacs-configure.log
log_exit ${PIPESTATUS[0]}

#
make -j4 | tee emacs-make.log
MAKE_EXIT_CODE=${PIPESTATUS[0]}
log_exit $MAKE_EXIT_CODE

#
if [ -z "$1" ]; then
    src/emacs -Q
    log_exit $?
fi
src/emacs --version
log_exit $?

if [ $EMACS_BRANCH_DIRNAME != "master" ]; then
    echo "Installing Emacs $EMACS_BRANCH_DIRNAME"
    make install | tee emacs-install.log
    log_exit ${PIPESTATUS[0]}
fi
cd -

if [ ${MAKE_EXIT_CODE} -eq 0 ]; then
    cd emacs
    git tag ${EMACS_BRANCH_DIRNAME_FULL}
    cd -
fi

EMACS_DESKTOP_FILE_DIR="/usr/local/share/applications"
mkdir -pv $EMACS_DESKTOP_FILE_DIR
EMACS_DESKTOP_FILE_DEFAULT="${EMACS_DESKTOP_FILE_DIR}/emacs.desktop"
EMACS_DESKTOP_FILE_NEW="${EMACS_DESKTOP_FILE_DIR}/emacs-${EMACS_BRANCH_DIRNAME_NEW}.desktop"
if [ ! -f  $EMACS_DESKTOP_FILE_DEFAULT ]; then
    cat <<EOF > $EMACS_DESKTOP_FILE_DEFAULT
[Desktop Entry]
Name=Emacs
GenericName=Text Editor
Comment=Edit text
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
Exec=emacs %F
Icon=emacs
Type=Application
Terminal=false
Categories=Development;TextEditor;
StartupWMClass=Emacs
Keywords=Text;Editor;
EOF
else
    cp -vf $EMACS_DESKTOP_FILE_DEFAULT $EMACS_DESKTOP_FILE_NEW
    sed -i.backup \
	-e "s/^Name=.*$/Name=Emacs (${EMACS_BRANCH_DIRNAME_NEW})/" \
	-e "s,^Exec=.*$,Exec=${EMACS_RUN_DIR}/bin/emacs -mm %F," \
	$EMACS_DESKTOP_FILE_NEW
fi

# ------------ org-free-desktop
if [ -d ${EMACS_RUN_DIR}/bin ]; then
    cat <<EOF > "lll-${EMACS_BRANCH_DIRNAME_FULL}.sh"
#!/bin/bash

source $HOME/.bashrc

# -fn "Monospace-16"
$EMACS_RUN_DIR/bin/emacs \
    --xrm="Emacs.pane.menubar.font: -*-*-medium-r-*-*-*-160-*-*-m-*-*-*" \
    -mm \$@
EOF
    chmod u+x "lll-${EMACS_BRANCH_DIRNAME_FULL}.sh"
    cp -v "lll-${EMACS_BRANCH_DIRNAME_FULL}.sh" ${EMACS_RUN_DIR}/..
    log_exit $?
    ln -svf "${EMACS_RUN_DIR}/../lll-${EMACS_BRANCH_DIRNAME_FULL}.sh" /usr/local/bin
    log_exit $?
    /usr/local/bin/lll-${EMACS_BRANCH_DIRNAME_FULL}.sh --version

    # emacsclient
    cat <<EOF > "lll-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh"
#!/bin/bash

$EMACS_RUN_DIR/bin/emacsclient \$@

EOF
    chmod u+x "lll-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh"
    cp -v "lll-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh" ${EMACS_RUN_DIR}/..
    log_exit $?
    ln -svf "${EMACS_RUN_DIR}/../lll-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh" /usr/local/bin
    log_exit $?
    /usr/local/bin/lll-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh --version
fi
# ------------ org-free-desktop

# ----------- NextStep
if [ -d emacs-${EMACS_BRANCH_DIRNAME_FULL}/nextstep/Emacs.app ]; then
    # NS Emacs.app
    cp -Rv emacs-${EMACS_BRANCH_DIRNAME_FULL}/nextstep/Emacs.app ${EMACS_RUN_DIR}
    cat <<EOF > "lll-ns-${EMACS_BRANCH_DIRNAME_FULL}.sh"
#!/bin/bash

# -fn "Monospace-16"
open ${EMACS_RUN_DIR}/Emacs.app --args -mm \$@

EOF
    chmod u+x "lll-ns-${EMACS_BRANCH_DIRNAME_FULL}.sh"
    cp -v "lll-ns-${EMACS_BRANCH_DIRNAME_FULL}.sh" ${EMACS_RUN_DIR}/..
    ln -svf "${EMACS_RUN_DIR}/../lll-ns-${EMACS_BRANCH_DIRNAME_FULL}.sh" /usr/local/bin
    log_exit $?

    # NS emacsclient
    cat <<EOF > "lll-ns-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh"
#!/bin/bash

${EMACS_RUN_DIR}/Emacs.app/Contents/MacOS/bin/emacsclient \$@

EOF
    chmod u+x "lll-ns-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh"
    cp -v "lll-ns-${EMACS_BRANCH_DIRNAME_FULL}-emacsclient.sh" ${EMACS_RUN_DIR}/..
    log_exit $?
fi
# ----------- NextStep
