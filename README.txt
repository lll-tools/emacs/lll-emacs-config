Luis Lain


1 Emacs Make and Configuration System (EMaCS)
=============================================

  Traditionally [GNU Emacs] uses the `.emacs' [initialization file] and
  the `.emacs.d'
  [DotEmacsDotD] directory, both located in the user `$HOME'
  directory. The [programmer itch]
  comes this time from the need to share the same configuration files
  for running different
  versions and builds of [GNU Emacs]. Current workarounds found were
  some scripts for
  renaming `.emacs.d' directory, let's better make [GNU Emacs] itself
  deal with this task.

  EMaCS will *backup* your current [initialization file] renaming it to
  `init-user-backup.el'.

  EMaCS will *overwrite* your current [initialization file] with the
  project's `src/init.el'.

  EMaCS will *create* a custom `user-emacs-directory' directory at run
  time with the value
  `$XDG_CONFIG_HOME/emacs/.emacs.<version>-<build>-<date>' for each
  instance of [GNU Emacs]
  runned, and will *copy* custom init files into it.

  The `user-emacs-directory' directory (any of `$HOME/.emacs.d' or the
  [XDG-compatible]
  `$XDG_CONFIG_HOME/emacs') that [GNU Emacs] [decides] to use is
  modified by EMaCS to store
  different user customizations for each version and build of [GNU
  Emacs].  EMaCS will always
  set the `user-emacs-directory' variable into the [XDG-compatible]
  directory
  `$XDG_CONFIG_HOME/emacs' (defaults to `$HOME/.config/emacs') with the
  value
  `.emacs.<emacs-version>-<build-number>-<build-date>'.


[GNU Emacs] <https://www.gnu.org/software/emacs/>

[initialization file]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html>

[DotEmacsDotD] <https://www.emacswiki.org/emacs/DotEmacsDotD>

[programmer itch] <http://www.catb.org/~esr/writings/taoup/>

[XDG-compatible]
<https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>

[decides]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Find-Init.html#Find-Init>

1.1 Running different versions and builds of Emacs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  The EMaCS [initialization file] `src/init.el' will be shared for
  running many different
  versions and builds of [GNU Emacs]. EMaCS provides the main [GNU
  Emacs] [initialization file]
  `src/init.el', that will be the new `user-init-file' config file for
  all [GNU Emacs]
  versions and builds in the system. The same [initialization file]
  `src/init.el' will be used
  with different `user-emacs-directory' drectories, all created at
  runtime relying on the
  version and build of the instance of [GNU Emacs] runned.

  We recommend against modifying the EMaCS [initialization file]
  `src/init.el', any
  customization may be done in the configuration file `custom-init.el'
  included in the
  specific `user-emacs-directory' directory.


[initialization file]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html>

[GNU Emacs] <https://www.gnu.org/software/emacs/>


1.2 Compiling Emacs from source code.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  EMaCS provides the `bin/emacs-compile.sh' script to compile [GNU
  Emacs] from source code.


[GNU Emacs] <https://www.gnu.org/software/emacs/>


2 Quick start
=============

2.1 EMaCS Clone
~~~~~~~~~~~~~~~

  EMaCS project is hosted in [gitlab.com]:

  - Git clone the EMaCS `lll-emacs-config' repository
    ,----
    | git clone https://gitlab.com/lll-tools/emacs/lll-emacs-config.git
    | cd lll-emacs-config
    | make help
    `----

  EMaCS provides a default `Makefile' with PHONY targets.

  - Execute `make' or `make help' for a list of the available options.
    ,----
    | make
    `----

  - Execute `make check-local' to search and show the existing [GNU
    Emacs] [initialization file]
    in the system and the action to be executed for installation of the
    `src/init.el'.
    ,----
    | make check-local
    `----

  - Execute `make info-local' to search and show the existing [GNU
    Emacs] config files in the system.
    ,----
    | make info-local
    `----


[gitlab.com] <https://gitlab.com/>

[GNU Emacs] <https://www.gnu.org/software/emacs/>

[initialization file]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html>


2.2 EMaCS Install
~~~~~~~~~~~~~~~~~

  *Install EMaCS*, copy the files needed to run the current [GNU Emacs]
   installed in the system
  using the EMaCS [initialization file] `src/init.el'.

  - Execute `make install-data-local':
    ,----
    | make install-data-local
    `----
    - *Backup* the current `user-init-file' (any of `$HOME/.emacs' or
       `$HOME/.emacs.el' or
      `$HOME/.emacs.d/init.el' or `$XDG_CONFIG_HOME/emacs/init.el') as
      `init-user-backup.el'
      in the same location of the current `user-init-file' found.
      - `$HOME/.emacs' -> `$HOME/init-user-backup.el'
      - `$HOME/.emacs.el' -> `$HOME/init-user-backup.el'
      - `$HOME/.emacs.d/init.el' -> `$HOME/.emacs.d/init-user-backup.el'
      - `$XDG_CONFIG_HOME/emacs/init.el' ->
        `$XDG_CONFIG_HOME/emacs/init-user-backup.el'

    - *Overwrite* the current `user-init-file' with the EMaCS
       [initialization file] `src/init.el'.
      - `src/init.el' -> `$HOME/.emacs'
      - `src/init.el' -> `$HOME/.emacs.el'
      - `src/init.el' -> `$HOME/.emacs.d/init.el'
      - `src/init.el' -> `$XDG_CONFIG_HOME/emacs/init.el'

    - *Create* the [XDG-compatible] directory `$XDG_CONFIG_HOME/emacs'
       (defaults to
      `$HOME/.config/emacs') that will be used as the root directory for
      all the EMaCS
      configurations. In the `'First Run'' of each version and build of
      [GNU Emacs] installed
      in the system.
            The `user-emacs-directory' of the specific version and
            build of [GNU Emacs] being
            runned will be created into this directory as the
            specific subdirectory
            `.emacs.<emacs-version>-<build-number>-<build-date>'
    - *Copy* the custom configuration init files into the
       [XDG-compatible] directory created.
      - `src/init-first-run.el' ->
        `$XDG_CONFIG_HOME/emacs/init-first-run.el'
      - `src/init-packages.el' ->
        `$XDG_CONFIG_HOME/emacs/init-packages.el'
      - `src/init-packages-custom.el' ->
        `$XDG_CONFIG_HOME/emacs/init-packages-custom.el'
      - `src/init-profile.el' ->
        `$XDG_CONFIG_HOME/emacs/init-profile.el'
      - `src/init-sources.el' ->
        `$XDG_CONFIG_HOME/emacs/init-sources.el'
      - `src/init-dev.el' -> `$XDG_CONFIG_HOME/emacs/init-dev.el'
      - `src/init-user.el' -> `$XDG_CONFIG_HOME/emacs/init-user.el'

  *Un-install EMaCS*, revert the [initialization file] to the previous
   `user-init-file' value
   restoring the backup file `init-user-backup.el' created in the
   installation process.
  - Execute `make clean-local' to revert the installation of EMaCS in
    the system.
    ,----
    | make clean-local
    `----
    - *Restore* the `init-user-backup.el' as the current
       `user-init-file'.
    - *Remove* the `init-user.el', if it exists.
    - The configuration files copied to the [XDG-compatible] directory
      are NOT deleted.
    - The custom `user-emacs-directory' created in the [XDG-compatible]
      directory during the
      `'First Run'' is NOT deleted.


[GNU Emacs] <https://www.gnu.org/software/emacs/>

[initialization file]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html>

[XDG-compatible]
<https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>


2.3 EMaCS First Run
~~~~~~~~~~~~~~~~~~~

  The first time that you run any version and build of [GNU Emacs]
  available in the system
  using the installed EMaCS [initialization file] `src/init-el', it will
  ask if the
  `init-user-backup.el' should be used.
  <file:doc/README-01.png>

  - *y*, EMaCS will copy the backup of the user current configuration
     `init-user-backup.el'
    to the file `init-user.el' and will load it at startup, using it
    with the
    `$XDG_CONFIG_HOME/emacs/.emacs.<emacs-version>-<build-number>-<build-date>'
    directory as
    `user-emacs-directory'. Any other custom init files provided by
    EMaCS will NOT be
    used. Answer *y* only if you just want to try the specific
    `user-emacs-directory' with
    your current configuration.

  - *n*, EMaCS will NOT load the user current configuration
     `init-user-backup.el' at
    startup, it will instead ask consecutively if other popular GNU
    Emacs configurations
    should be used:

    - [SPACEMACS]

    - [DOOM]

    - [PURCELL]

  - *n*, Finally, if you say NO to all the choices before, EMaCS will
     NOT load the user
    current configuration `init-user-backup.el' at startup, and you will
    be using instead
    the EMaCS custom init files installed in the [XDG-compatible]
    directory. Answer *n* to try
    the full EMaCS features.
    - *Define* `user-emacs-directory/custom.el' file for customizations.
    - *Load* `$XDG_CONFIG_HOME/emacs/init-first-run.el'
    - *Create* `user-emacs-directory/custom-init.el'
    - If the version of [GNU Emacs] is < 28,
      - *Copy* `$XDG_CONFIG_HOME/emacs/init-packages-custom.el' to
        `$XDG_CONFIG_HOME/emacs/init-<emacs-version>-<build-number>-<build-date>.el'
      - *Insert* load `init-packages.el' in
         `user-emacs-directory/custom-init.el'
      - *Insert* load
         `init-<emacs-version>-<build-number>-<build-date>.el' in
        `user-emacs-directory/custom-init.el'
    - If the version of [GNU Emacs] is not < 28,
      - *Insert* load `$XDG_CONFIG_HOME/emacs/init-sources.el' in
         `user-emacs-directory/custom-init.el'
    - *Insert* load `$XDG_CONFIG_HOME/emacs/init-dev.el' in
       `user-emacs-directory/custom-init.el'
    - *Insert* load `$XDG_CONFIG_HOME/emacs/init-profile.el' in
       `user-emacs-directory/custom-init.el'
    - *Load* `user-emacs-directory/custom-init.el'
    - *Create* `user-emacs-directory/custom.el' file saving first run
       customizations.


[GNU Emacs] <https://www.gnu.org/software/emacs/>

[initialization file]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html>

[SPACEMACS] <https://github.com/syl20bnr/spacemacs>

[DOOM] <https://github.com/doomemacs/doomemacs>

[PURCELL] <https://github.com/purcell/emacs.d>

[XDG-compatible]
<https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>


2.4 EMaCS Make (GNU Emacs Clone)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Execute `make install-exec-local' to clone the [GNU Emacs] source
    code repository.
    ,----
    | make install-exec-local
    `----
    - Git clone [GNU Emacs] source code in `./emacs' directory.

  - Execute `make install' to do both `make install-data-local' and
    `make install-exec-local'.
    This is only available after using the provided autotools script
    `autobuild.sh'.
    ,----
    | ./autobuild.sh
    | make install
    `----


[GNU Emacs] <https://www.gnu.org/software/emacs/>


3 EMaCS run details
===================

  GNU Emacs [customizations] for standard options.

  EMaCS installation `make install-data-local' will *copy* the
  `init-first-run.el' file in
  the XDG-compatible directory.

  EMaCS `init.el' will *load* the `init-user.el' file if it exists.

  EMaCS `init.el' will *search* for the `init-first-run.el' file with
  common customizations
  to *import* them and *create* a new `custom.el' file in the
  `user-emacs-directory'
  directory the first time that any new version and build of [GNU Emacs]
  is runned.

  EMaCS `init.el' will *load* the `custom.el' file located in the
  `user-emacs-directory'
  created.
  - custom.el, location
  - default.el, (site-lisp) location
  - site-start.el, (site-lisp) location

  EMaCS `init.el' will *create* the `custom-init.el' file in the
  `user-emacs-directory'
  directory for the user to add specific customizations.

  Only files located in the specific `user-emacs-directory' should be
  modified.

  The directory `user-emacs-directory' may be completely deleted anytime
  and EMaCS will regenerate
  all the dependencies and packages with a fresh install.


[customizations]
<https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Customizations.html>

[GNU Emacs] <https://www.gnu.org/software/emacs/>


4 EMaCS Configuration
=====================

4.1 User init
~~~~~~~~~~~~~

  When the file `init-user.el' is found, EMaCS custom init files are NOT
  loaded.
  Use the current user configuration with the specific
  `user-emacs-directory'.


4.2 First Run Customizations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  When the file `init-first-run.el' is found, defined customizations are
  saved to the
  `custom.el' file.
  Use `M-x customize' to add new.


4.3 Packages (using Melpa)
~~~~~~~~~~~~~~~~~~~~~~~~~~

  When the file `init-packages.el' is found, the load is added to the
  `custom-init.el' file.
  Add your preferred packages in the file `custom-init.el'.


4.4 Packages (using Source)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

  When the file `init-sources.el' is found, the load is added to the
  `custom-init.el' file.
  Add the packages used from source, NOT from melpa.


4.5 Development
~~~~~~~~~~~~~~~

  When the file `init-dev.el' is found, the load is added to the
  `custom-init.el' file.
  Define your Elisp modules (el modes) under development in the file
  `custom-init.el'.


4.6 Profile
~~~~~~~~~~~

  When the file `init-profile.el' is found, the load is added to the
  `custom-init.el' file.
  Define your key bindings, macros and alias in the file
  `custom-init.el'.


5 EMaCS Make GNU Emacs
======================

  - Run the compilation script.
    ,----
    | ./bin/emacs-compile.sh
    `----

  - Run new compiled Emacs.
    ,----
    | ./emacs/src/emacs
    `----

  - Create .desktop launcher
    ,----
    | kioclient5 exec /usr/share/applications/emacs-lsp.desktop
    | gtk-launch emacs-lsp
    `----


6 Similar projects
==================

  <https://github.com/syl20bnr/spacemacs>
  <https://github.com/doomemacs/doomemacs>
  <https://github.com/purcell/emacs.d>
  <https://github.com/purcell/setup-emacs>
  <https://github.com/purcell/nix-emacs-ci>
  <https://github.com/jbpros/emacs-setup/>
  <https://github.com/flavorjones/emacs.d>
  <https://github.com/technomancy/emacs-starter-kit/>
  <https://github.com/rdallasgray/pallet>
  <https://github.com/melpa/package-build>
  <https://github.com/cask/cask>


6.1 Run GNU Emacs as Docker container
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  <https://hub.docker.com/r/silex/emacs>
  ,----
  | docker run -it --rm silex/emacs
  | # WARN: this comes with security issues
  | xhost +local:root
  | docker run -it --rm \
  |        -e DISPLAY \
  |        -v /tmp/.X11-unix:/tmp/.X11-unix \
  |        silex/emacs
  `----


7 Contributions
===============

  <https://gitlab.com/lll-tools/emacs/lll-emacs-config/-/issues>


8 Release life-cycle
====================

  1. *checkout* `dev' branch
  2. *create* and *checkout* new `release/vx.y.z' branch
  3. *reset* (mixed) `release/vx.y.z' branch to `vx.y' branch
     1. Discard "`dev' branch only" files and `ChangeLog' file
     2. Update `ChangeLog' file with changes to show in `master' branch
  4. *commit* `release/vx.y.z' branch
  5. *checkout* `vx.y' branch
  6. *merge* (probably ff) `release/vx.y.z' branch into `vx.y' branch
  7. *checkout* `dev' branch
  8. *tag* `dev' branch with `tags/vx.y.z'
  9. Repeat this loop (Steps 1-8) any times before merge into `master'
     branch
  10. *checkout* `master' branch
  11. *merge* (--squash) `vx.y' branch
      1. Fix merge conflicts
      2. Update `ChangeLog' file with changes to show in `master' branch
  12. *commit* `master' branch
      1. Fix squash merge message (release/vx.y.z)
  13. *checkout* `vx.y' branch
  14. *merge* `master' branch into `vx.y'
      1. Should be empty or only ChangeLog if modified in step 11
  15. *checkout* `master' branch
  16. *push* `master' branch into origin
  17. *checkout* `dev' branch
      1. Fork release x.y (when required)
  18. Bump version x.y.z to next release
      1. Edit `src/init.el'
      2. Edit `.gitlab-ci.yml'


8.1 Only when new fork from previous release
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  1. *merge* (--no-commit --no-ff) `vx.y' branch into `dev' branch
     1. Remove ChangeLog file from merge (ours)
     2. Commit merge (should be empty)
