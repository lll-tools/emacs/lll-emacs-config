;;; init-packages-majormodes --- Custom packages
;;; Commentary:
;;; Code:

;; ------------ MAJOR MODES
(use-package ox-gfm
  :after org ;; (featurep 'org)
  :config
  (eval-after-load "org" '(require 'ox-gfm nil t)))

(use-package ob-go
  :after org
  :config
  (eval-after-load "org" '(require 'ob-go nil t))
  :custom
  (org-babel-do-load-languages 'org-babel-load-languages '((go . t)))
  )

(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "markdown"))

(use-package dockerfile-mode
  :commands (dockerfile-mode)
  :mode (("Dockerfile\\'" . dockerfile-mode)))

(use-package terraform-mode
  :commands (terraform-mode)
  :mode (("\\.tf\\'" . terraform-mode)))

(use-package groovy-mode)

(use-package jenkinsfile-mode
  :after groovy-mode
  :mode (("Jenkinsfile\\'" . jenkinsfile-mode)))

(use-package gitlab-ci-mode)

;; GO
(use-package go-mode)

;; PYTHON
(use-package pyvenv
  :init
  (setenv "WORKON_HOME" "/usr/local/src/python"))

;; RUBY
;; In order to get goto-step-definition to work, you must install the
;; ruby_parser gem (version 2.0.x) and cucumber-gherkin (version
;; 2.11.8)
;; gem install ruby_parser --version "~> 3.14.2"
;; gem install cucumber-gherkin --version 14.0.1
(use-package feature-mode
  :mode ("\\.feature\\'" . feature-mode)
  ;; :config
  ;; (setq feature-default-language "fi")
  ;; (setq feature-default-i18n-file "/path/to/gherkin/gem/i18n.yml")
  ;; (setq feature-step-search-path "features/**/*steps.rb")
  ;; (setq feature-step-search-gems-path "gems/ruby/*/gems/*/**/*steps.rb")
  )

(provide 'init-packages-majormodes)
;;; init-packages-majormodes.el ends here
