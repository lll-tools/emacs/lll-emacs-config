
;;; Code:

;; use-package
(setq use-package-source (expand-file-name "src/use-package" user-emacs-directory))
(unless (directory-name-p use-package-source)
  (shell-command (concat "cd "
			 (shell-quote-argument (expand-file-name user-emacs-directory))
			 "; mkdir -p src; cd src"
			 "; git clone https://github.com/jwiegley/use-package.git"
			 "; cd use-package; make"))) ;; compile
;; LOAD_PATH = -L use-package-source > config.mk

(add-to-list 'load-path use-package-source)
(require 'use-package)

(with-eval-after-load 'info
  (info-initialize)
  (add-to-list 'Info-directory-list
     	       (file-name-as-directory use-package-source)))

;; dash source https://github.com/magnars/dash.el
(setq dash-source (expand-file-name "src/dash.el" user-emacs-directory))
(unless (file-exists-p dash-source)
  (shell-command (concat "cd "
			 (shell-quote-argument (expand-file-name user-emacs-directory))
			 "; mkdir -p src; cd src"
			 "; git clone https://github.com/magnars/dash.el.git")))
(add-to-list 'load-path dash-source)
(require 'dash)

;; transient source https://github.com/magit/transient
(setq transient-source (expand-file-name "src/transient" user-emacs-directory))
(unless (file-exists-p transient-source)
  (shell-command (concat "cd "
			 (shell-quote-argument (expand-file-name user-emacs-directory))
			 "; mkdir -p src; cd src"
			 "; git clone https://github.com/magit/transient.git")))
(add-to-list 'load-path (expand-file-name "lisp" transient-source))
(require 'transient)

;; with-editor source https://github.com/magit/with-editor
(setq with-editor-source (expand-file-name "src/with-editor" user-emacs-directory))
(unless (file-exists-p with-editor-source)
  (shell-command (concat "cd "
			 (shell-quote-argument (expand-file-name user-emacs-directory))
			 "; mkdir -p src; cd src"
			 "; git clone https://github.com/magit/with-editor.git")))
(add-to-list 'load-path with-editor-source)
(require 'with-editor)

;; Magit
(setq magit-source (expand-file-name "src/magit" user-emacs-directory))
(unless (file-exists-p magit-source)
  (shell-command (concat "cd "
			 (shell-quote-argument (expand-file-name user-emacs-directory))
			 "; mkdir -p src; cd src"
			 "; git clone https://github.com/magit/magit.git"
			 "; cd magit; make")))
(add-to-list 'load-path (expand-file-name "lisp" magit-source))
(require 'magit)
(with-eval-after-load 'info
  (info-initialize)
  (add-to-list 'Info-directory-list
	       (expand-file-name "Documentation" magit-source))
  )

;; (setq treepy-source (expand-file-name "src/treepy.el" user-emacs-directory))
;; (unless (file-exists-p treepy-source)
;;   (shell-command (concat "cd "
;; 			 (shell-quote-argument (expand-file-name user-emacs-directory))
;; 			 "; mkdir -p src; cd src"
;; 			 "; git clone https://github.com/volrath/treepy.el.git"
;; 			 "; cp -f ./treepy.el/treepy.el ../elpa/treepy-0.1.1/")))
;; (add-to-list 'load-path treepy-source)
;; (require 'treepy)
