;; Development projects added to load-path

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

;; (let ((default-directory "~/.emacs.d/plugins/"))
;;   (normal-top-level-add-subdirs-to-load-path))

;; Globals for lll-world
;; (require 'lll-world)
;; (global-set-key (kbd "C-c w") 'lll-world)
;; (use-package lll-world
;;   :ensure t
;;   :bind ("C-c w" . lll-world))

;; ;; lll-dummy
;; (when (y-or-n-p "Load lll-Dummy module? ")
;;   (add-to-list 'load-path
;; 	       (expand-file-name
;; 		"/usr/local/src/luislain/lll-tools/emacs/lll-emacs-dummy/src"))
;;   (require 'lll-dummy)
;;   (global-set-key (kbd "C-c y") 'lll-dummy))

;; ;; gsettigns
;; (use-package dash :ensure t)
;; (when (y-or-n-p "Load lll-gsettings module? ")
;;   (add-to-list 'load-path
;; 	       (expand-file-name
;; 		"/usr/local/src/luislain/lll-tools/emacs/lll-emacs-gsettings/src"))
;;   (require 'lll-gsettings)
;;   (global-set-key (kbd "C-c y") 'lll-gsettings))

;; ;; kubectl
;; (when (y-or-n-p "Load lll-kubectl module? ")
;;   (add-to-list 'load-path
;; 	       (expand-file-name
;; 		"/usr/local/src/luislain/lll-tools/emacs/lll-emacs-kubectl/src"))
;;   (require 'lll-kubectl)
;;   (global-set-key (kbd "C-c k") 'lll-kubectl))

;; ;; aws
;; (when (y-or-n-p "Load lll-aws module? ")
;;   (add-to-list 'load-path
;; 	       (expand-file-name
;; 		"/usr/local/src/luislain/lll-tools/emacs/lll-emacs-aws/src"))
;;   (require 'lll-aws)
;;   (global-set-key (kbd "C-c w") 'lll-aws)
;;   (setenv "AWS_CLI_AUTO_PROMPT"		"on-partial")
;;   ;; (setenv "AWS_DEFAULT_OUTPUT"		"json")
;;   ;; (setenv "AWS_DEFAULT_REGION"		"eu-west-1")
;;   ;; (setenv "AWS_REGION"			"eu-west-1")
;;   ;; (setenv "AWS_PROFILE"			"")
;;   ;; (setenv "AWS_ACCESS_KEY_ID"		"")
;;   ;; (setenv "AWS_SECRET_ACCESS_KEY"	"")
;;   ;; (setenv "AWS_SESSION_TOKEN"		"")
;;   ;; (setenv "AWS_SHARED_CREDENTIALS_FILE"	"")
;;   ;; (setenv "AWS_CONFIG_FILE"		"")
;;   )

;; ;; lll-cmd
;; (when (y-or-n-p "Load lll-cmd module? ")
;;   (add-to-list 'load-path
;; 	       (expand-file-name
;; 		"/usr/local/src/luislain/lll-tools/emacs/lll-emacs-cmd/src"))
;;   (require 'lll-cmd)
;;   (global-set-key (kbd "C-c r") 'lll-cmd)
;;   )
