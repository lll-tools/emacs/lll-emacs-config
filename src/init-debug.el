
;; setq lll-init-string-before in site-start.el file
;; setq lll-init-string-after in default.el file
(setq lll-init-string (format "Version (%s) build (%s) Default directory (%s) User directory (%s) User init file (%s) Custom file (%s) Auto save prefix (%s) Custom theme dir (%s)"
			      emacs-version emacs-build-number default-directory user-emacs-directory user-init-file custom-file auto-save-list-file-prefix custom-theme-directory))

(eval-after-load "org" '(debug))
