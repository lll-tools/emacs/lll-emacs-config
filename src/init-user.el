;;; init-user.el  --- Summary

;;; Commentary:

;; https://github.com/purcell/emacs.d/issues/340
;; https://www.spacemacs.org/doc/DOCUMENTATION.html
;; https://github.com/syl20bnr/spacemacs/issues/11585

;;; Code:

;; (setq load-prefer-newer t)
;; Remove ".gz" suffix
(setq load-file-rep-suffixes '(""))

(load-file (concat user-emacs-directory "init.el"))
;; (setq spacemacs-start-directory user-emacs-directory)
;; (load-file (concat spacemacs-start-directory "init.el"))

(provide 'init-user)
;;; init-user.el ends here
