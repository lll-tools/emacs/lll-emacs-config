;;; early-init.el --- EMaCS Early Init File -*- no-byte-compile: t -*-

;; Copyright (C) 2000-2022 luislain.com

;; Author: Luis Lain <luislain@luislain.com>
;; Homepage: https://gitlab.com/lll-tools/emacs/lll-emacs-config
;; Maintainer: Luis L. Lazaro <lll@luislain.com>

;; This file is not part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Before Emacs 27, the init file was responsible for initializing the package
;; manager by calling `package-initialize'.  Emacs 27 changed the default
;; behavior: It now calls `package-initialize' before loading the init file.
;; This behavior would prevent EMaCS's own package initialization from
;; running.  However, Emacs 27 also loads the "early init" file (this file)
;; before it initializes the package manager, and EMaCS can use this early
;; init file to prevent Emacs from initializing the package manager.  (See
;; <http://git.savannah.gnu.org/cgit/emacs.git/commit/?id=24acb31c04b4048b85311d794e600ecd7ce60d3b>.)
;;
;; Earlier Emacs versions do not load the early init file and do not initialize
;; the package manager before loading the init file, so this file is neither
;; needed nor loaded on those versions.

;;; Code:
(setq package-enable-at-startup nil)
;;
