;;; init-profile.el --- User customizations configuration file

;; Copyright (C) 2020 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: config
;; Package: lll-emacs-config

;; This file is NOT part of GNU Emacs.

;; This file is part of EMaCS.

;; Emacs Make and Configuration System (EMaCS) is free software; you can
;; redistribute it and/or modify it under the terms of the GNU General
;; Public License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.

;; Emacs Make and Configuration System (EMaCS) is distributed in the hope
;; that it will be useful, but WITHOUT ANY WARRANTY; without even the
;; implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with EMaCS.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; User customizations, key bindings, macros and alias.

;;; Code:

;;; USER CUSTOMIZATIONS
;; ;; Git user
;; (customize-set-variable 'user-full-name		"User Name")
;; (customize-set-variable 'user-mail-address	"username@example.com")
;; ;; ChangeLog user
;; (customize-set-variable 'add-log-full-name	"User Name")
;; (customize-set-variable 'add-log-mailing-address "username@example.com")

;; (toggle-frame-maximized)

;;; KEY BINDINGS
;; (define-key global-map (kbd "C-s-z") nil)
;; (global-unset-key (kbd "C-s-z"))
;; (define-key global-map (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "s-,") 'indent-region)
(global-set-key (kbd "s-b") 'buffer-menu)
(global-set-key (kbd "s-k") 'kill-current-buffer)
(global-set-key (kbd "s-x") 'async-shell-command)

;; Global key binding for MacOSX
(when (and (lll-is-osx)
	   (> emacs-major-version 27))
  (global-set-key (kbd "s-<right>") 'ns-next-frame)
  (global-set-key (kbd "s-<left>") 'ns-prev-frame)
  )

;;; MACROS
(fset 'lll-sort-buffer-list
   (kmacro-lambda-form [?\C-x ?\C-b ?\C-x ?o ?\C-s ?t ?e ?r ?a ?c ?t ?i ?o return ?S ?S] 0 "%d"))
(global-set-key (kbd "s-v") 'lll-sort-buffer-list)

(fset 'lll-kubectl-swap-output
   (kmacro-lambda-form [?\C-x ?0 ?\C-x ?o ?\C-x ?b return ?\M-<] 0 "%d"))
(global-set-key (kbd "s-i") 'lll-kubectl-swap-output)

;; (setq ls-lisp-use-insert-directory-program nil)
;; (require 'ls-lisp)

;;; HOOKS

;;; ALIAS
(defun lll-flush-blank-lines ()
  "Delete blank lines."
  (interactive)
  (message "Flush %d blank lines in buffer %s from %d to %d"
	   (flush-lines "^ *$") ;; (point) (point-max) t))
	   (current-buffer) (point) (point-max)))

(defun lll-flush-blank-lines-region ()
  "Flush blank lines in the region of the current buffer."
  (interactive)
  (save-excursion
    ;; (exchange-point-and-mark)
    (flush-lines "^ *$" (region-beginning) (region-end) t))
  )

;; https://github.com/antonj/Highlight-Indentation-for-Emacs
;; https://github.com/DarthFennec/highlight-indent-guides
(defun lll-aj-toggle-fold ()
  "Toggle fold all lines larger than indentation on current line."
  (interactive)
  (let ((col 1))
    (save-excursion
      (back-to-indentation)
      (setq col (+ 1 (current-column)))
      (set-selective-display
       (if selective-display nil (or col 1))))))
(global-set-key (kbd "C-c t") 'lll-aj-toggle-fold)

;; ENV
;; (getenv "AWS_PROFILE")
;; (setenv "AWS_PROFILE" "luislain@dev")
;; (getenv "DOCKER_HOST")
;; (setenv "DOCKER_HOST" "tcp://0.0.0.0:2375")

;; ;; https://www.reddit.com/r/emacs/comments/mc82yk/flickering_emacs_on_macos/
;; ;;(https://github.com/emacs-mirror/emacs/blob/master/etc/PROBLEMS)
;; ;; *** Display artifacts on GUI frames on X-based systems.
;; ;; This is known to be caused by using double-buffering (which is enabled`
;; ;; by default in Emacs 26 and later). The artifacts typically appear
;; ;; after commands that cause Emacs to scroll the display.
;; ;; You can disable double-buffering by evaluating the following form:`
;; (modify-all-frames-parameters '((inhibit-double-buffering . t)))
;; ;; Note that disabling double-buffering will cause flickering of the
;; ;; display in some situations.
