;;; init-packages-autocompletion --- Custom packages
;;; Commentary:
;;; Code:

(use-package yasnippet
  :config (yas-global-mode))

(use-package company)
;; (setq company-idle-delay 0)
;; (setq company-minimum-prefix-length 1)

(defvar company-mode/enable-yas t
  "Enable yasnippet for all backends.")

(defun company-mode/backend-with-yas (backend)
  "Company enable yas in BACKEND."
  (if (or (not company-mode/enable-yas)
	  (and (listp backend)
	       (member 'company-yasnippet backend)))
      backend
    (append (if (consp backend) backend (list backend))
	    '(:with company-yasnippet))))

(setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))

(use-package flycheck
  :config (global-flycheck-mode))

;; (use-package helm
;;   :config (helm-mode))

;; https://oremacs.com/swiper/
;; (use-package counsel)
;; (ivy-mode 1)
;; (setq ivy-use-virtual-buffers t)
;; (setq ivy-count-format "(%d/%d) ")

(provide 'init-packages-autocompletion)
;;; init-packages-autocompletion.el ends here
