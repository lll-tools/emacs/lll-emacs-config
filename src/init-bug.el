(require 'package)
(package-initialize)

(customize-set-variable
 'package-archives (add-to-list
		    'package-archives
		    (cons "melpa" "https://melpa.org/packages/") t))

(customize-set-variable 'custom-enabled-themes '(tango-dark))
(customize-save-customized)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package tango-dark-theme
  :ensure t
  :config
  (load-theme 'tango-dark t))

(use-package org
  :ensure nil
  :defer t
  :bind (("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
         ("C-c c" . org-capture)
	 ))
