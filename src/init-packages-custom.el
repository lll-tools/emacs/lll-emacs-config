;;; init-packages-custom --- Custom packages
;;; Commentary:
;;; Code:

(load-file (expand-file-name "init-packages-majormodes.el" lll-xdg-emacs-directory))
(load-file (expand-file-name "init-packages-autocompletion.el" lll-xdg-emacs-directory))
;; (load-file (expand-file-name "init-packages-lsp.el" lll-xdg-emacs-directory))

;; ;; https://github.com/stsquad/emacs_chrome
;; (use-package edit-server
;;   :commands edit-server-start
;;   :init (if after-init-time
;;             (edit-server-start)
;;           (add-hook 'after-init-hook
;;                     #'(lambda() (edit-server-start))))
;;   :config (setq edit-server-new-frame-alist
;;                 '((name . "Edit with Emacs FRAME")
;;                   (top . 200)
;;                   (left . 200)
;;                   (width . 80)
;;                   (height . 25)
;;                   (minibuffer . t)
;;                   (menu-bar-lines . t)
;;                   (window-system . x))))
;; (add-hook 'edit-server-start-hook 'markdown-mode)

(provide 'init-packages-custom)
;;; init-packages-custom.el ends here
