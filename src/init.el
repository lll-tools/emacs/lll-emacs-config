;;; init.el --- main configuration file of GNU Emacs	-*- lexical-binding: t; -*-

;; Copyright (C) 2000-2022 luislain.com

;; Author: Luis Lain <luislain@luislain.com>
;; Homepage: https://gitlab.com/lll-tools/emacs/lll-emacs-config
;; Maintainer: Luis L. Lazaro <lll@luislain.com>
;; Keywords: config tool

;; Package: lll-emacs-config
;; Version: 0.1.14
;; Package-Version: 0.1.14
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; SPDX-License-Identifier: GPL-3.0-or-later

;; Emacs Make and Configuration System (EMaCS) is free software; you can
;; redistribute it and/or modify it under the terms of the GNU General
;; Public License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.

;; Emacs Make and Configuration System (EMaCS) is distributed in the hope
;; that it will be useful, but WITHOUT ANY WARRANTY; without even the
;; implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with (EMaCS).  If not, see <http://www.gnu.org/licenses/>.

;; This file is NOT part of GNU Emacs.

;; This file is part of Emacs Make and Configuration System (EMaCS).

;;; Commentary:

;; Using the same `user-init-file' (init.el) file with different versions of GNU Emacs
;; Creating a specific `user-emacs-directory' based on GNU Emacs version

;;; Code:

;; loadup.el
;; startup.el

;; L396 auto-save-list-file-prefix <- user-emacs-directory
;; normal-top-level
;; -> L538 startup--xdg-or-homedot (.emacs.d) -> user-emacs-directory
;; -> L548 load-path <- data-directory ../lisp
;; -> L677 command-line
;; -> L1224 startup--load-user-init-file (early-init)
;; -> L1369 startup--load-user-init-file (site-start & init)
;; user-emacs-directory -> startup-init-directory

;; early-init.el
;; Any customization before this point should go in site-start.el file (site-run-file)

;; user-real-login-name
;; invocation-directory

(defconst lll-user-init-directory
  (expand-file-name (file-name-as-directory (file-name-directory user-init-file)))
  "The directory used for the `user-init-file' file.")

;; User current init file
(defvar lll-user-init-file
  (expand-file-name "init-user.el" lll-user-init-directory)
  "The complete path to the file with the profile of the user configuration.")

;; User current init file backup
(defconst lll-user-init-file-backup
  (expand-file-name "init-user-backup.el" lll-user-init-directory)
  "The complete path to the file storing the previous user configuration.")

;; Load debug file
(defconst lll-debug-file
  (expand-file-name "init-debug.el" lll-user-init-directory)
  "The complete path of the file with the lll-emacs-config debug options.")

(if (file-exists-p lll-debug-file) (load-file lll-debug-file))

;; https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
;; XDG_CONFIG_HOME Should default to $HOME/.config.
;; TODO: (make-symbolic-link ".config/emacs" "~/.emacs.d")
;; Check compatibility with Emacs 26 and lower
(if (boundp 'startup--xdg-config-home-emacs)
    (defvaralias 'lll-xdg-emacs-directory 'startup--xdg-config-home-emacs
      "XDG compatible directory based on `startup--xdg-config-home-emacs'")
  (defconst lll-xdg-emacs-directory
    (let ((xdg-config-home (getenv "XDG_CONFIG_HOME")))
      (if xdg-config-home
	  (expand-file-name (file-name-as-directory
			      (concat (file-name-as-directory xdg-config-home)
				      "emacs")))
	;;startup--xdg-config-default
	(expand-file-name (file-name-as-directory "~/.config/emacs"))))
    "The path to the XDG-compatible directory for GNU Emacs."))

(make-directory lll-xdg-emacs-directory t)

;; TODO: Use chemacs command line arg to load a specific profile
;; https://github.com/plexus/chemacs
;; (command-line-args) invocation-directory installation-directory

;; GNU Emacs variant
(defconst lll-emacs-variant
  (concat emacs-version
	  (if (boundp 'emacs-build-number)
	      (concat "-" (number-to-string emacs-build-number)))
	  (if (boundp 'emacs-build-time)
	      (concat "-" (format-time-string "%Y%m%d" emacs-build-time))))
  "The string defining the variant of the GNU Emacs instance running.

This string will be used to create the `user-emacs-directory'.")

(setq frame-title-format '("" "Emacs@" lll-emacs-variant "@ [ %b ] "))

(defconst lll-variant-init-file
  (expand-file-name (concat "init-" lll-emacs-variant ".el")
		    lll-xdg-emacs-directory)
  "Specific init file for each variant.

Located outside the `user-emacs-directory' for persistence when deleting the variant directory.
Use custom-packges.el and custom-profile.el for the variant volatile configurations. ")

;; 2befb4f0a1494f699f56215d5f28ba055663d881
;; Author:     Paul Eggert <eggert@cs.ucla.edu>
;; AuthorDate: Sat Aug 31 14:47:04 2019 -0700
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=583#56
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=15539#40
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=37354

;; Define Custom file and set user-emacs-directory as a side effect
(defconst lll-custom-file
  (if (not (boundp 'user-emacs-directory))
      (expand-file-name "custom.el" lll-user-init-directory)
    ;; Customize user-emacs-directory, probably introduced at Emacs 22.1
    (setq-default user-emacs-directory
		  (expand-file-name
		   (file-name-as-directory (concat ".emacs." lll-emacs-variant))
		   lll-xdg-emacs-directory))
    (make-directory user-emacs-directory t)
    ;; Reload default value of dumped variables
    (custom-reevaluate-setting 'auto-save-list-file-prefix)
    (custom-reevaluate-setting 'custom-theme-directory)
    (custom-reevaluate-setting 'abbreviated-home-dir)
    (custom-reevaluate-setting 'abbrev-file-name)
    (if (> emacs-major-version 27) (custom-reevaluate-setting 'package-user-dir))
    ;; TODO: (setq package-user-dir (expand-file-name ""))
    ;; (setq package-user-dir (locate-user-emacs-file
    ;;                         (concat (file-name-as-directory "elpa")
    ;; 				    emacs-version)))
    ;; package-directory-list
    ;; TODO: (customize user-cache-directory and user-ramdisk-directory
    ;; Set current user init, at Emacs 23.1
    (setq-default lll-user-init-file (locate-user-emacs-file "init-user.el"))
    ;; Customize custom-file relative to user-emacs-directory
    (locate-user-emacs-file "custom.el"))
  "The complete path to the file storing customizations.

Sets `user-emacs-directory' as a side effect.
Updates `lll-user-init-file' value based on `user-emacs-directory' as a side effect.")

;; Load current user configuration (lll-user-init-file)
;; -OR-
;; Load EMaCS config files (lll-custom-file)
(if (file-exists-p lll-user-init-file)
    (load-file lll-user-init-file)
  ;; Load EMaCS config files
  (eval-and-compile (require 'package))

  (defun lll-is-osx ()
    "Return non-nil when OSX system."
    (and (string= system-type "darwin")
	 (or (featurep 'ns)
	     (featurep 'mac)
	     (string= (framep (selected-frame)) "ns"))))

  (defun lll-is-python3 ()
    "Return non-nil when python3 command available in Emacs."
    (not (> (length (split-string (shell-command-to-string "python3 --version")))
	    2)))

  (defun lll-is-npm ()
    "Return non-nil when npm command available in Emacs."
    (not (> (length (split-string (shell-command-to-string "npm --version")))
	    1)))

  (defun lll-is-transient ()
    "Return non-nil when transient version available in Emacs is > 0.2.0."
    (> (car (package-desc-version (cadr (assq 'transient package-alist))))
       20200225)) ;; commit a269614

  ;; Start First Run process: one of "lll-custom-file" or "lll-user-init-file" will be created
  (unless (file-exists-p lll-custom-file)
    (message "--- Start First Run process.")
    ;; 1. ask to the user for using the current config
    (if (and (file-exists-p lll-user-init-file-backup)
	     (y-or-n-p "Do you want to use your current configuration ? "))
	(progn
	  ;; TODO: Check that lll-user-init-directory is NOT the user $HOME
	  (copy-directory lll-user-init-directory user-emacs-directory t t t)
	  (copy-file lll-user-init-file-backup lll-user-init-file 0)
	  (message "Use existing `lll-user-init-file' user configuration.")
	  )
      ;; IF user does NOT want to use the current config
      (defconst lll-user-init (expand-file-name "init-user.el" lll-xdg-emacs-directory))
      ;; 2. ask to the user for using SPACEMACS config
      (if (y-or-n-p "Do you want to use SPACEMACS configuration ? ")
	  (progn
	    ;; TODO: Git clone SPACEMACS repo in user-emacs-directory
	    ;; (copy-directory lll-user-init-directory user-emacs-directory t t t)
	    (pop-to-buffer (make-comint-in-buffer
		    "*lll-init-spacemacs*" nil
		    "git" nil "clone"
		    "https://github.com/syl20bnr/spacemacs" user-emacs-directory))
	    (y-or-n-p "Has *git clone* SPACEMACS finished succesfully ? ")
	    ;; TODO: Exit when error in git clone
	    (copy-file lll-user-init lll-user-init-file 0)
	    (message "Using SPACEMACS configuration.")
	    )
	;; IF user does NOT want to use SPACEMACS config
	;; 3. create the custom-file
	;; IF custom-file does NOT exists it will be created at the end of first run with save-customized
	(with-temp-file lll-custom-file
	  (insert ";; Do manual customizations in here if M-x customize cannot be used") (newline 3)
	  (insert ";; Do NOT modify lines below this point ......................") (newline 2)
	  )
	(defconst lll-first-run-file (expand-file-name "init-first-run.el" lll-xdg-emacs-directory))
	)
      )
    ;; Check again if the user-init-file has been created in the first run
    (when (file-exists-p lll-user-init-file)
      (message "--- Using USER init files (First Run).")
      (load-file lll-user-init-file)
      )
    )

  ;; Load custom file if the lll-user-init-file has NOT been created (and loaded) in the first run
  (unless (file-exists-p lll-user-init-file)
    (message "--- Using EMaCS init files.")
    ;; Customize custom-file.
    (customize-set-variable 'custom-file lll-custom-file)
    ;; (setq custom-file lll-custom-file)
    ;; Load custom-file.
    (load-file custom-file)
    ;; Load first-run customizations
    (when (boundp 'lll-first-run-file)
      (if (file-exists-p lll-first-run-file)
	  (load-file lll-first-run-file) ; Load first-run-customizations from config-file
	(warn "Missign first run init file: %S" lll-first-run-file))
      )
    ;; Disable default in early-init.el "package-enable-at-startup" value for Emacs 27 and above
    (package-initialize)

    (defconst lll-custom-init-file (locate-user-emacs-file "custom-init.el"))
    (defconst lll-packages-init (expand-file-name "init-packages.el" lll-xdg-emacs-directory))
    (defconst lll-custom-packages-init (expand-file-name "init-packages-custom.el" lll-xdg-emacs-directory))
    (defconst lll-sources-init (expand-file-name "init-sources.el" lll-xdg-emacs-directory))
    (defconst lll-development-init (expand-file-name "init-dev.el" lll-xdg-emacs-directory))
    (defconst lll-profile-init (expand-file-name "init-profile.el" lll-xdg-emacs-directory))

    ;; Create EMaCS packages file
    (unless (file-exists-p lll-custom-init-file)
      (message "--- Creating custom-init init file.")
      (with-temp-file lll-custom-init-file
	(insert ";; Load init files.") (newline))
      ;; Use melpa for Emacs 28 and below
      (if (< emacs-major-version 29)
	  (when (file-exists-p lll-packages-init)
	    (message "--- Using custom-packages init file.")
	    (unless (file-exists-p lll-variant-init-file)
	      (copy-file lll-custom-packages-init lll-variant-init-file))
	    (with-temp-buffer
	      (insert ";; Load default init-packages.") (newline)
	      (insert "(load-file lll-packages-init)") (newline 2)
	      (insert "(load-file lll-variant-init-file)") (newline 2)
	      (append-to-file nil nil lll-custom-init-file)
	      )
	    )
	;; Use git for Emacs 29 and above
	(when (file-exists-p lll-sources-init)
	  (message "--- Using custom-sources init file.")
	  (with-temp-buffer
	    (insert ";; Load default init-sources.") (newline)
	    (insert "(load-file lll-sources-init)") (newline 2)
	    (append-to-file nil nil lll-custom-init-file)
	    )
	  )
	)
      ;;
      (when (file-exists-p lll-development-init)
	(message "--- Using custom-dev init file.")
	(with-temp-buffer
	  (insert ";; Load default init-development.") (newline)
	  (insert ";;(load-file lll-development-init)") (newline 2)
	  (append-to-file nil nil lll-custom-init-file)
	  )
	)
      ;;
      (when (file-exists-p lll-profile-init)
	(message "--- Using custom-profile init file.")
	(with-temp-buffer
	  (insert ";; Load default init-profile customizations.") (newline)
	  (insert "(load-file lll-profile-init)") (newline 2)
	  (insert (concat ";; Add below specific customizations for "
			  lll-emacs-variant))
	  (newline 3)
	  (append-to-file nil nil lll-custom-init-file)
	  )
	)
      )
    ;; Load EMaCS packages file
    (load-file lll-custom-init-file)
    )
  )

;; Write in custom-file the first-run-customizations using customize-set-variable
(customize-save-customized)
;; TODO: create theme with customizations
;; Any customization after this point should go in default.el file
;; Customize variables using (M-x customize) and (M-x org-customize) whenever possible

(provide 'init)
;;; init.el ends here
