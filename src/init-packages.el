;;; init-packages.el --- packages configuration file of GNU Emacs

;; Copyright (C) 2020 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: config
;; Package: lll-emacs-config

;; This file is NOT part of GNU Emacs.

;; This file is part of EMaCS.

;; Emacs Make and Configuration System (EMaCS) is free software; you can
;; redistribute it and/or modify it under the terms of the GNU General
;; Public License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.

;; Emacs Make and Configuration System (EMaCS) is distributed in the hope
;; that it will be useful, but WITHOUT ANY WARRANTY; without even the
;; implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with EMaCS.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;; Stable packages
(message "--- Init Packages")
(eval-when-compile (require 'package))

(if (< emacs-major-version 27)
    (package-initialize))

(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(eval-and-compile (require 'use-package))
(message "Use-package version: %s" use-package-version)

;; no-littering
;; NOTE: If you want to move everything out of the ~/.emacs.d folder
;; reliably, set `user-emacs-directory` before loading no-littering!
;;(setq user-emacs-directory "~/.cache/emacs")
;;(use-package no-littering)
;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
;;(setq auto-save-file-name-transforms
;;       `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

;; Org mode
(message "Org version: %s (before use-package)" (org-version))
(use-package org
  :bind (("C-c l" . org-store-link)
         ("C-c c" . org-capture)
         ("C-c a" . org-agenda)
         ("C-c b" . org-iswitchb)
	 )
  :custom
  (org-log-done 'time)
  (calendar-latitude 41.6) ;; set it to your location
  (calendar-longitude 0.9) ;; Usable for M-x `sunrise-sunset' or in `org-agenda'
  ;; (org-babel-load-file (expand-file-name "init.org" user-emacs-directory))
  ;; (org-export-backends (quote (ascii html icalendar latex md odt)))
  ;; (defun lll-org-confirm-babel-evaluate (lang body)
  ;;   (not (or (string= lang "ditaa") (string= lang "sql") )))
  ;; (setq org-confirm-babel-evaluate #'lll-org-confirm-babel-evaluate)
  ;; (if (and (fboundp 'native-comp-available-p) (native-comp-available-p)) ...)
  ;; :load-path (concat user-emacs-directory "elpa/org-" org-version "/")
  )
(message "Org version: %s (after use-package)" (org-version))

(use-package which-key
  :config (which-key-mode)
  )

(use-package ace-jump-mode
  :bind (("C-c SPC" . ace-jump-mode))
  )

(when (> emacs-major-version 26)
  (use-package magit
    :bind (("C-x g" . magit-status)
	   ("C-x M-g" . magit-dispatch))
    )
  (use-package docker
    :bind (("C-c d" . docker))
    )
  )

(provide 'init-packages)
;;; init-packages.el ends here
