;; Emacs under version 27 require to call (package-initialize) in init file
;; Emacs above version 27 will set package-enable-at-startup to nil
;; Check package-load-list value
;; (package--user-selected-p 'package)
;; (customize-set-variable 'package-selected-packages ())
;; (package-initialize)
;; (setq package-enable-at-startup nil)
;; (setq package-check-signature "allow-unsigned")

;; TODO: Test if there are some customized values not saved (load user-init) or create first-run
;; (when (get symbol 'customized-value)

;; Indent customization
;; (defun my-indent-region (N)
;;   (interactive "p")
;;   (if (use-region-p)
;;       (progn (indent-rigidly (region-beginning) (region-end) (* N 4))
;;              (setq deactivate-mark nil))
;;     (self-insert-command N)))
;; (defun my-unindent-region (N)
;;   (interactive "p")
;;   (if (use-region-p)
;;       (progn (indent-rigidly (region-beginning) (region-end) (* N -4))
;; 	     (setq deactivate-mark nil))
;;     (self-insert-command N)))
;; (global-set-key ">" 'my-indent-region)
;; (global-set-key "<" 'my-unindent-region)

;; Gitlab related packages
;; (use-package forge
;;   :after magit)

;; (use-package gitlab
;;   :ensure f
;;   ;; :config
;;   ;; (setq gitlab-host "https://gitlab.com/lll-tools/emacs/lll-emacs-config"
;;   ;; 	gitlab-token-id "V2uAo2uNY6")
;;   )

;;https://www.emacswiki.org/emacs/BackupDirectory
;;https://www.gnu.org/software/emacs/manual/html_node/tramp/Auto_002dsave-and-Backup.html
;; https://emacs.stackexchange.com/questions/33/put-all-backups-into-one-backup-folder
;; ;; Put backup files neatly away                                                 
;; (let ((backup-dir "~/tmp/emacs/backups")
;;       (auto-saves-dir "~/tmp/emacs/auto-saves/"))
;;   (dolist (dir (list backup-dir auto-saves-dir))
;;     (when (not (file-directory-p dir))
;;       (make-directory dir t)))
;;   (setq backup-directory-alist `(("." . ,backup-dir))
;;         auto-save-file-name-transforms `((".*" ,auto-saves-dir t))
;;         auto-save-list-file-prefix (concat auto-saves-dir ".saves-")
;;         tramp-backup-directory-alist `((".*" . ,backup-dir))
;;         tramp-auto-save-directory auto-saves-dir))

;;https://stackoverflow.com/questions/3893727/setting-emacs-tramp-to-store-local-backups
;; (defvar user-temporary-file-directory  
;;   (concat "/tmp/" user-login-name "/emacs_backup/"))  
;; (make-directory user-temporary-file-directory t)  
;; (setq make-backup-files t)  
;; (setq backup-by-copying t)  
;; (setq version-control t)  
;; (setq delete-old-versions t)  
;; (setq kept-new-versions 10)  
;; (setq backup-directory-alist `(("." . ,user-temporary-file-directory)))  
;; (setq tramp-backup-directory-alist backup-directory-alist)  
;; (setq tramp-auto-save-directory user-temporary-file-directory)  
;; (setq auto-save-list-file-prefix
;;       (concat user-temporary-file-directory ".auto-saves-"))  
;; (setq auto-save-file-name-transforms  
;;       `((".*" ,user-temporary-file-directory t)))

;; (setq backup-by-copying t    ; Don't delink hardlinks                           
;;       delete-old-versions t  ; Clean up the backups                             
;;       version-control t      ; Use version numbers on backups,                  
;;       kept-new-versions 5    ; keep some new versions                           
;;       kept-old-versions 2)   ; and some old ones, too

;; Set auto-save-file-name-transforms to nil to save auto-saved files
;; to the same directory as the original file.
;; Alternatively, set the user option tramp-auto-save-directory
;; to direct all auto saves to that location.
;; tramp-auto-save-directory
;; tramp-handle-find-backup-file-name
;; auto-save-file-name-transforms
;; auto-save-list-file-prefix
;; temporary-file-directory
;; make-backup-file-name-function

;; Org customizations
;; (customize-set-variable 'org-return-follows-link t)
;; (when (file-directory-p (expand-file-name "~/Documents/"))
;;     (customize-set-variable 'org-agenda-files '("~/Documents/")))
;; TODO: (customize-set-variable 'org-export-backends ())
;; TODO: (customize-set-variable 'org-babel-load-languages
;; 			(add-to-list
;; 			 (add-to-list
;; 			  (add-to-list
;; 			   'org-babel-load-languages
;; 			   '("shell" . t) t)
;; 			  '("python" . t) t)
;; 			 '("latex" . t) t))

;; (use-package ox-gfm
;;   :ensure t
;;   :after org ;; (featurep 'org)
;;   :config
;;   (eval-after-load "org" '(require 'ox-gfm nil t)))

;; (use-package ox-gfm
;;   :defer 3
;;   :after org)
;; (use-package org
;; 	     :custom
;; 	     (org-babel-load-languages ()))

;; (use-package markdown-mode
;;   :ensure t
;;   :commands (markdown-mode gfm-mode)
;;   :mode (("README\\.md\\'" . gfm-mode)
;;          ("\\.md\\'" . gfm-mode)
;;          ("\\.markdown\\'" . markdown-mode))
;;   :init (setq markdown-command "markdown"))

;; (setq markdown-command "grip --export"))
;; (setq markdown-command "pandoc -c file:///home/beaujean/.emacs.d/github-pandoc.css --from markdown_github -t html5 --mathjax --highlight-style pygments --standalone")
;; ;; Use visual-line-mode in gfm-mode
;; (defun my-gfm-mode-hook ()
;;   (visual-line-mode 1))
;; (add-hook 'gfm-mode-hook 'my-gfm-mode-hook)

;; (use-package dockerfile-mode
;;   :ensure t
;;   :commands (dockerfile-mode)
;;   :mode (("Dockerfile\\'" . dockerfile-mode)))

;; (require 'dockerfile-mode)
;; (add-to-list 'load-path "/your/path/to/dockerfile-mode/")
;; (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))
;; ## -*- docker-image-name: "your-image-name-here" -*-
;; (put 'dockerfile-image-name 'safe-local-variable #'stringp)
;; (setq dockerfile-mode-command "docker")

;; Reading EPUB
;; (use-package nov
;;   :ensure t
;;   :mode ("\\.epub\\'" . nov-mode))
;; ;; (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
;; ;; interpreter-mode-alist

;; (use-package lua-mode
;;   :ensure t
;;   :mode ("\\.lua\\'" . lua-mode))
;; ;; (autoload 'lua-mode "lua-mode" "Lua editing mode." t)
;; ;; (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
;; ;; (add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

;; (use-package auto-complete
;;   :ensure t
;;   :config
;;   (ac-config-default))

;; http://company-mode.github.io/
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Completion-in-Buffers.html

;; (use-package company
;;   :ensure t)

;; (use-package company-c-headers
;;   :ensure t)

;; (use-package company-box
;;   :hook (company-mode . company-box-mode))

;; (use-package python-mode
;;   :ensure t)

;; (use-package pyvenv
;;   :ensure t)

;; (use-package web-mode
;;   :ensure t)

;; (use-package elpy
;;   :ensure t
;;   :init
;;   (elpy-enable))

;; Error (use-package): auctex/:catch: Loading file /home/llain/.config/emacs/27.0.91-1/elpa/auctex-12.2.4/auctex.elc failed to provide feature ‘auctex’
;; https://github.com/jwiegley/use-package/issues/379#issuecomment-258217014
;; (use-package auctex
;;   :defer t
;;   :ensure t
;;   :config
;;   (setq TeX-auto-save t)
;;   (setq TeX-parse-self t)
;;   ;; (setq-default TeX-master nil)
;;   ;; (require 'reftex)
;;   ;; (add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ; with AUCTeX LaTeX mode
;;   ;; (add-hook 'latex-mode-hook 'turn-on-reftex)   ; with Emacs latex mode
;;   )

;; (use-package tex-site
;;   :ensure auctex
;;   :mode ("\\.tex\\'" . latex-mode)
;;   :config
;;   (setq TeX-auto-save t)
;;   (setq TeX-parse-self t)
;;   ;; (setq-default TeX-master nil)
;;   ;; (add-hook 'LaTeX-mode-hook
;;   ;;           (lambda ()
;;   ;;             (rainbow-delimiters-mode)
;;   ;;             (company-mode)
;;   ;;             (smartparens-mode)
;;   ;;             (turn-on-reftex)
;;   ;;             (setq reftex-plug-into-AUCTeX t)
;;   ;;             (reftex-isearch-minor-mode)
;;   ;;             (setq TeX-PDF-mode t)
;;   ;;             (setq TeX-source-correlate-method 'synctex)
;;   ;;             (setq TeX-source-correlate-start-server t)))
;;   )

;; (use-package reftex
;;   :ensure t
;;   :defer t
;;   :config
;;   (setq reftex-cite-prompt-optional-args t)); Prompt for empty optional arguments in cite

;; https://www.emacswiki.org/emacs/TransposeWindows
;; (unless (package-installed-p 'buffer-move)
;;   ;; only fetch the archives if you don't have package installed
;;   (package-refresh-contents)
;;   (package-install 'buffer-move))
;; (require 'buffer-move)
;; "M-x b-ri" "M-x b-le"
;; (global-set-key (kbd "<C-S-up>")     'buf-move-up)
;; (global-set-key (kbd "<C-S-down>")   'buf-move-down)
;; (global-set-key (kbd "<C-S-left>")   'buf-move-left)
;; (global-set-key (kbd "<C-S-right>")  'buf-move-right)

;; LSP MODE
;; ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
;; (setq lsp-keymap-prefix "s-l")

;; (use-package lsp-mode
;;     :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
;;             (XXX-mode . lsp)
;;             ;; if you want which-key integration
;;             (lsp-mode . lsp-enable-which-key-integration))
;;     :commands lsp)

;; :init
;; ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
;; (setq lsp-keymap-prefix "C-c l")

;; To defer LSP server startup (and DidOpen notifications) until the buffer is visible you can use lsp-deferred instead of lsp:
;; (use-package lsp-mode
;;     :hook (XXX-mode . lsp-deferred)
;;     :commands (lsp lsp-deferred))

;;(setq lsp-python-ms-executable "/usr/local/bin/Microsoft.Python.LanguageServer")
;;(setq lsp-python-ms-executable (executable-find "Microsoft.Python.LanguageServer"))
;; (setq lsp-python-ms-executable (executable-find "python-language-server"))
;; lsp-python-ms-extra-paths
;; chmod u+x .config/emacs/.emacs.26.3-2-20200326/.cache/lsp/mspyls/Microsoft.Python.LanguageServer

;; https://github.com/emacs-lsp/lsp-docker

;; https://github.com/emacs-lsp/helm-lsp/

;; https://github.com/bash-lsp/bash-language-server
;; npm i -g bash-language-server

;; https://www.npmjs.com/package/dockerfile-language-server-nodejs
;; https://github.com/rcjsuen/dockerfile-language-server-nodejs#installation-instructions
;; https://emacs-lsp.github.io/lsp-mode/page/lsp-dockerfile/

;; ;; optionally
;; (use-package lsp-ui :commands lsp-ui-mode)
;; ;; if you are helm user
;; (use-package helm-lsp :commands helm-lsp-workspace-symbol)
;; ;; if you are ivy user
;; (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
;; (use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; ;; optionally if you want to use debugger
;; (use-package dap-mode)
;; ;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; ;; optional if you want which-key integration
;; (use-package which-key
;;     :config
;;     (which-key-mode))

;; TREEMACS
;; (use-package treemacs
;;   :ensure t
;;   :defer t
;;   :init
;;   (with-eval-after-load 'winum
;;     (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
;;   :config
;;   (progn
;;     (setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
;;           treemacs-deferred-git-apply-delay      0.5
;;           treemacs-directory-name-transformer    #'identity
;;           treemacs-display-in-side-window        t
;;           treemacs-eldoc-display                 t
;;           treemacs-file-event-delay              5000
;;           treemacs-file-extension-regex          treemacs-last-period-regex-value
;;           treemacs-file-follow-delay             0.2
;;           treemacs-file-name-transformer         #'identity
;;           treemacs-follow-after-init             t
;;           treemacs-git-command-pipe              ""
;;           treemacs-goto-tag-strategy             'refetch-index
;;           treemacs-indentation                   2
;;           treemacs-indentation-string            " "
;;           treemacs-is-never-other-window         nil
;;           treemacs-max-git-entries               5000
;;           treemacs-missing-project-action        'ask
;;           treemacs-move-forward-on-expand        nil
;;           treemacs-no-png-images                 nil
;;           treemacs-no-delete-other-windows       t
;;           treemacs-project-follow-cleanup        nil
;;           treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
;;           treemacs-position                      'left
;;           treemacs-recenter-distance             0.1
;;           treemacs-recenter-after-file-follow    nil
;;           treemacs-recenter-after-tag-follow     nil
;;           treemacs-recenter-after-project-jump   'always
;;           treemacs-recenter-after-project-expand 'on-distance
;;           treemacs-show-cursor                   nil
;;           treemacs-show-hidden-files             t
;;           treemacs-silent-filewatch              nil
;;           treemacs-silent-refresh                nil
;;           treemacs-sorting                       'alphabetic-asc
;;           treemacs-space-between-root-nodes      t
;;           treemacs-tag-follow-cleanup            t
;;           treemacs-tag-follow-delay              1.5
;;           treemacs-user-mode-line-format         nil
;;           treemacs-user-header-line-format       nil
;;           treemacs-width                         35)

;;     ;; The default width and height of the icons is 22 pixels. If you are
;;     ;; using a Hi-DPI display, uncomment this to double the icon size.
;;     ;;(treemacs-resize-icons 44)

;;     (treemacs-follow-mode t)
;;     (treemacs-filewatch-mode t)
;;     (treemacs-fringe-indicator-mode t)
;;     (pcase (cons (not (null (executable-find "git")))
;;                  (not (null treemacs-python-executable)))
;;       (`(t . t)
;;        (treemacs-git-mode 'deferred))
;;       (`(t . _)
;;        (treemacs-git-mode 'simple))))
;;   :bind
;;   (:map global-map
;;         ("M-0"       . treemacs-select-window)
;;         ("C-x t 1"   . treemacs-delete-other-windows)
;;         ("C-x t t"   . treemacs)
;;         ("C-x t B"   . treemacs-bookmark)
;;         ("C-x t C-t" . treemacs-find-file)
;;         ("C-x t M-t" . treemacs-find-tag)))

;; (use-package treemacs-evil
;;   :after treemacs evil
;;   :ensure t)

;; (use-package treemacs-projectile
;;   :after treemacs projectile
;;   :ensure t)

;; (use-package treemacs-icons-dired
;;   :after treemacs dired
;;   :ensure t
;;   :config (treemacs-icons-dired-mode))

;; (use-package treemacs-magit
;;   :after treemacs magit
;;   :ensure t)

;; (use-package treemacs-persp
;;   :after treemacs persp-mode
;;   :ensure t
;;   :config (treemacs-set-scope-type 'Perspectives))

;; ;; https://gist.github.com/sebastiencs/a16ea58b2d23e2ea52f62fcce70f4073
;; https://github.com/company-mode/company-mode/blob/master/company-yasnippet.el
;; https://emacs.stackexchange.com/questions/10431/get-company-to-show-suggestions-for-yasnippet-names
;; Add yasnippet support for all company backends
;; https://github.com/syl20bnr/spacemacs/pull/179

;; https://emacs.stackexchange.com/questions/10431/get-company-to-show-suggestions-for-yasnippet-names
;; ;; With this code, yasnippet will expand the snippet if company didn't complete the word
;; ;; replace company-complete-common with company-complete if you're using it
;; (defvar my-company-point nil)
;; (advice-add 'company-complete-common
;; 	    :before (lambda () (setq my-company-point (point))))
;; (advice-add 'company-complete-common
;; 	    :after (lambda () (when (equal my-company-point (point))
;; 				(yas-expand))))

;; ;; https://emacs-lsp.github.io/lsp-mode/page/installation/
;; ;; https://github.com/emacs-lsp/lsp-mode
;; (use-package lsp-mode
;;   :init
;;   ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
;;   (setq lsp-keymap-prefix "C-c l")
;;   :config
;;   (setq lsp-log-io t)
;;   :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
;;          (go-mode . lsp)
;; 	 (python-mode . lsp)
;;          ;; if you want which-key integration
;;          (lsp-mode . lsp-enable-which-key-integration))
;;   :commands lsp)

;; https://emacs.stackexchange.com/questions/46361/no-org-babel-execute-function-for-go
;; https://github.com/pope/ob-go

;; (lsp-register-client
;;  (make-lsp-client
;;   :new-connection (lsp-stdio-connection '("/usr/local/var/lib/code/extensions/hashicorp.terraform-2.11.0/lsp/terraform-ls" "serve"))
;;   :major-modes '(terraform-mode)
;;   :server-id 'terraform-ls))

;; ;; (add-hook 'terraform-mode-hook #'lsp)
;; (add-hook 'terraform-mode-hook #'lsp-deferred)

;; ;; Python Microsoft
;; ;; https://github.com/emacs-lsp/lsp-python-ms
;; ;; https://github.com/Microsoft/python-language-server
;; ;; /usr/local/bin/python/python-language-server/output/bin/Release/linux-x64/publish/Microsoft.Python.LanguageServer
;; (use-package lsp-python-ms
;;   :ensure t
;;   :init
;;   (setq lsp-python-ms-auto-install-server t)
;;   ;;(setq lsp-python-ms-executable "/usr/local/bin/Microsoft.Python.LanguageServer")
;;   ;;(setq lsp-python-ms-executable (executable-find "Microsoft.Python.LanguageServer"))
;;   ;;(setq lsp-python-ms-executable (executable-find "python-language-server"))
;;   ;;chmod u+x .emacs.d/.cache/lsp/mspyls/Microsoft.Python.LanguageServer
;;   ;;(lsp-python-ms-extra-paths)
;;   :hook (python-mode . (lambda ()
;; 			 (require 'lsp-python-ms)
;; 			 (lsp))))  ; or lsp-deferred

;; Python Jedi
;; https://github.com/python-lsp/python-lsp-server
;; https://github.com/davidhalter/jedi
;; https://jedi.readthedocs.io/en/latest/docs/installation.html

;; ;; optionally
;; (use-package lsp-ui :commands lsp-ui-mode)
;; ;; if you are helm user
;; (use-package helm-lsp :commands helm-lsp-workspace-symbol)
;; ;; if you are ivy user
;; (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
;; (use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; ;; optionally if you want to use debugger
;; (use-package dap-mode)
;; ;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; (use-package php-mode
;;   :ensure t)

;; (use-package lua-mode
;;   :ensure t
;;   :mode ("\\.lua\\'" . lua-mode))

;; (use-package gnuplot
;;   :ensure t
;;   :commands (gnuplot-mode)
;;   :mode (("\\.gp$" . gnuplot-mode)
;; 	 ("\\.dem$" . gnuplot-mode)))

;;   ;; :after graphql
;;   (use-package magit
;;     :ensure t
;;     :bind (("C-x g" . magit-status)
;; 	   ("C-x M-g" . magit-dispatch))
;;     )

;;   (use-package docker
;;     :ensure t
;;     :bind ("C-c d" . docker))

;;   (use-package nov
;;     :ensure t
;;     :mode ("\\.epub\\'" . nov-mode))

;;   (use-package csv-mode
;;     :ensure t
;;     :mode ("\\.[Cc][Ss][Vv]\\'" . csv-mode))

;;   )

;; flycheck
;; https://www.flycheck.org/en/latest/user/quickstart.html
;; pip install pylint
;; npm install eslint

;; https://stackoverflow.com/questions/13866848/how-to-save-a-list-of-all-the-installed-packages-in-emacs-24
;; (defun list-packages-and-versions ()
;;   "Returns a list of all installed packages and their versions"
;;   (mapcar
;;    (lambda (pkg)
;;      `(,pkg ,(package-desc-version
;;                 (cadr (assq pkg package-alist)))))
;;    package-activated-list))

;; for pyvenv
;; (defvar *python-current-env*)

;; (defun python-find-env (project-root)
;;   "Find the python project env directory, inside PROJECT-ROOT."
;;   (require 'projectile)
;;   (car (-intersection (mapcar (lambda (path) (f-join project-root path))
;; 			      (list "env" ".env"))
;; 		      (f-directories project-root))))

;; (add-hook 'python-mode-hook
;; 	  (lambda ()
;; 	    (require 'projectile)
;; 	    (let* ((root (projectile-project-root))
;; 		   (env (python-find-env root)))
;; 	      (when (and env
;; 			 (not (equal env *python-current-env*)))
;; 		(progn
;; 		  (setf *python-current-env* env)
;; 		  (pyvenv-activate env)
;; 		  (message "Current python env: %s" *python-current-env*))
;; 		))))
;; ;;))

;; (use-package gitignore-mode
;;   :ensure t)

;; (use-package graphql
;;   :ensure t)

;; (use-package treepy
;;   :ensure t) 

;; (when (or (and (boundp 'lll-ssl-available)
;; 	       lll-ssl-available)
;; 	  (and (boundp 'lll-ssl-available-first-run)
;; 	       lll-ssl-available-first-run))
;; (when (and (lll-is-npm)
;; 	     (lll-is-transient) ;; transient version > 0.2.0
;; 	     (lll-is-python3)
;; 	     (> emacs-major-version 25)
;; 	     )


;; ;; (defvar lll-async-shell-command-hook nil
;; ;;   ".")
;; ;; (defun lll-async-shell-command ()
;; ;;   "Make shell interactive before calling `async-shell-command'."
;; ;;   (interactive)
;; ;;   (run-hooks 'lll-async-shell-command-hook)
;; ;;   (call-interactively #'async-shell-command)
;; ;;   )
;; ;; (defun lll-make-shell-interactive ()
;; ;;   "."
;; ;;   (setq shell-command-switch "-ic")
;; ;;   )
;; ;; (add-hook 'lll-async-shell-command-hook 'lll-make-shell-interactive)

;; (defun lll-async-shell-command ()
;;   "Make shell interactive before calling `async-shell-command'."
;;   (interactive)
;;   (setq shell-command-switch "-ic")
;;   (call-interactively #'async-shell-command)
;;   ;; (call-process "/bin/bash" nil t nil "-c")
;;   ;; (customize-set-variable exec-path)
;;   )

;; ;; current-minibuffer-command
;; (defun pre-command-hook-fn ()
;;   "."
;;   (if (not (string= current-minibuffer-command "async-shell-command"))
;;       (setq shell-command-switch "-c")
;;     (setq shell-command-switch "-ic")
;;     ;; (setq lll-path-from-shell (shell-command-to-string "$SHELL -c 'set | grep ^PATH='"))
;;     ;; (setq exec-path (split-string lll-path-from-shell path-separator))
;;     ;; (setq exec-path (append '("/usr/local/var/lib/xdg/config/nvm/versions/node/v18.15.0/bin")
;;     ;; 			     exec-path))
;;     )
;;   )

;; (add-hook 'minibuffer-setup-hook 'pre-command-hook-fn)

;; (add-hook 'pre-command-hook 'pre-command-hook-fn)

;; (minibuffer-with-setup-hook
;;     'pre-command-hook-fn
;;   (call-interactively #'async-shell-command)
;;   )

;; ;; (advice-add 'read-from-minibuffer :before 'before-first-read-from-minibuffer)
;; ;; (advice-add 
;; ;;    'display-buffer    ; function to be advised
;; ;;    :before            ; advice runs first
;; ;;    (lambda (&rest r) (delete-other-windows)) ; advising function, this must have the same argument list as the main function, in this case all absorbed into a list "r"
;; ;;    '((name . "test"))) ; convenient name for identifying or removing this advice later
;; ;; (advice-remove 'display-buffer "test")

;; (defun lll-bash ()
;;   "Execute interactive command."
;;   (interactive)
;;   ;; (start-process NAME BUFFER PROGRAM &rest PROGRAM-ARGS)
;;   (start-process "lll-bash-process" "*lll-bash-process*" "/bin/bash")
;;   )

;; (put 'after-save-hook 'safe-local-variable
;;      (lambda (value) (equal value '(org-babel-tangle t))))

;; ;; temporarily set shell-command-switch to -ic until function completes
;; (defun wrap-shell-command (func &rest args)
;;   "FUNC ARGS."
;;   (let ((current-shell-command-switch shell-command-switch))
;;     (setq shell-command-switch "-ic")
;;     (apply func args)
;;     (setq shell-command-switch current-shell-command-switch)))
;; ;; usage
;; (wrap-shell-command 'shell-command "func-define-in-bashrc" "other" "args")
