;;; init-packages-lsp --- Provide LSP packages
;;; Commentary:
;;; Code:

;; Projectile

(use-package projectile
  :config
  (projectile-mode +1)
  ;; ;; Recommended keymap prefix on macOS
  ;; (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  ;; Recommended keymap prefix on Windows/Linux
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  )
;; (setq projectile-completion-system 'ido)

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c s")
  :hook (
	 (sh-mode . lsp)
	 (python-mode . lsp)
	 (go-mode . lsp-deferred)
	 (terraform-mode . lsp-deferred)
	 (lsp-mode . lsp-enable-which-key-integration))
  )

(use-package lsp-ui)

;; Python
(use-package lsp-pyright
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp-deferred))))  ; or lsp

;; (when (or (and (boundp 'lll-ssl-available)
;; 	       lll-ssl-available)
;; 	  (and (boundp 'lll-ssl-available-first-run)
;; 	       lll-ssl-available-first-run))


;;   (when (and (boundp 'lll-ssl-available-first-run)
;; 	     lll-ssl-available-first-run)
;;     (lsp-install-server t 'bash-ls)
;;     (lsp-install-server t 'ts-ls)
;;     (lsp-install-server t 'json-ls)
;;     (lsp-install-server t 'yamlls)
;;     ;; (lsp-install-server t 'mspyls)
;;     ;; (lsp-install-server t 'clangd)
;;     )
;;   ;; )

;; https://emacs-lsp.github.io/lsp-mode/page/lsp-yaml/

;; Terraform --------------------
;; (setq lsp-log-io t)
;; (setq lsp-completion-enable-additional-text-edit nil))
;; (lsp-register-client
;;  (make-lsp-client
;;   :new-connection (lsp-stdio-connection '("/usr/local/var/lib/code/extensions/hashicorp.terraform-2.11.0/lsp/terraform-ls" "serve"))
;;   :major-modes '(terraform-mode)
;;   :server-id 'terraform-ls))
;; (add-hook 'terraform-mode-hook #'lsp-deferred)

;; ;; Go - lsp-mode ----------
;; ;; Set up before-save hooks to format buffer and add/delete imports.
;; (defun lsp-go-install-save-hooks ()
;;   (add-hook 'before-save-hook #'lsp-format-buffer t t)
;;   (add-hook 'before-save-hook #'lsp-organize-imports t t))
;; (add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
;; ;; Start LSP Mode and YASnippet mode
;; (add-hook 'go-mode-hook #'lsp-deferred)
;; (add-hook 'go-mode-hook #'yas-minor-mode)

;; LSP JAVA ---------------
;; https://www.reddit.com/r/emacs/comments/jfsoj5/finally_lsp_java_is_working/
;; https://github.com/emacs-lsp/lsp-java
;; (use-package projectile)
;; (use-package hydra)
;; (use-package lsp-java :config (add-hook 'java-mode-hook 'lsp))
;; (use-package dap-mode :after lsp-mode :config (dap-auto-configure-mode))
;; (use-package dap-java :ensure nil)

;; (use-package lsp-treemacs)
;; (use-package helm-lsp)
